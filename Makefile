CXXFLAGS= -I ./ -std=c++17
LDFLAGS= -lstdc++fs
SRC= $(wildcard *.cpp)
OBJDIR= .
OBJ= $(foreach src, $(SRC), $(OBJDIR)/$(notdir $(src:%.cpp=%.o)))
gnu64: build/bin/duck-gen
CXXFLAGS_gnu64= -I src
LDFLAGS_gnu64=
SRC_gnu64= $(wildcard src/*.cpp)
OBJDIR_gnu64= build/obj
OBJ_gnu64= $(foreach src, $(SRC_gnu64), $(OBJDIR_gnu64)/$(notdir $(src:%.cpp=%.o)))
run-gnu64: build/bin/duck-gen
	./$^
build/bin/duck-gen:$(OBJ) $(OBJ_gnu64) $(wildcard src/*.hpp)
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_gnu64) $(OBJ) $(OBJ_gnu64) $(LDFLAGS) $(LDFLAGS_gnu64) -o $@
install-gnu64: build/bin/duck-gen
	cp -vr $^ /usr/local/bin
	cp -vr templates /usr/local/share/duck-gen
uninstall-gnu64:
	rm -vir /usr/local/bin/duck-gen
	rm -vir /usr/local/share/duck-gen/templates
test: build/bin/duck-gen-test
CXXFLAGS_test= -I test -I src
LDFLAGS_test=
SRC_test= $(wildcard test/*.cpp) src/Parser.cpp src/FileReader.cpp src/FileWriter.cpp src/Template.cpp src/Generator.cpp
OBJDIR_test= build/obj_test
OBJ_test= $(foreach src, $(SRC_test), $(OBJDIR_test)/$(notdir $(src:%.cpp=%.o)))
run-test: build/bin/duck-gen-test
	./$^
build/bin/duck-gen-test:$(OBJ) $(OBJ_test) $(wildcard test/*.hpp) $(wildcard src/*.hpp)
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_test) $(OBJ) $(OBJ_test) $(LDFLAGS) $(LDFLAGS_test) -o $@
install-test: build/bin/duck-gen-test
	cp -vr $^ /usr/local/bin
	cp -vr templates /usr/local/share/duck-gen
uninstall-test:
	rm -vir /usr/local/bin/duck-gen-test
	rm -vir /usr/local/share/duck-gen/templates
$(OBJDIR)/%.o:%.cpp
	$(CXX) -c $(CXXFLAGS) $^ $(LDFLAGS) -o $@
$(OBJDIR_gnu64)/%.o:src/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_gnu64) $^ $(LDFLAGS) $(LDFLAGS_gnu64) -o $@
$(OBJDIR_test)/%.o:test/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_test) $^ $(LDFLAGS) $(LDFLAGS_test) -o $@
$(OBJDIR_test)/%.o:src/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_test) $^ $(LDFLAGS) $(LDFLAGS_test) -o $@
$(OBJDIR_test)/%.o:src/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_test) $^ $(LDFLAGS) $(LDFLAGS_test) -o $@
$(OBJDIR_test)/%.o:src/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_test) $^ $(LDFLAGS) $(LDFLAGS_test) -o $@
$(OBJDIR_test)/%.o:src/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_test) $^ $(LDFLAGS) $(LDFLAGS_test) -o $@
$(OBJDIR_test)/%.o:src/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXFLAGS_test) $^ $(LDFLAGS) $(LDFLAGS_test) -o $@
check:
	cppcheck --enable=all $(SRC) $(SRC_gnu64) $(SRC_test)
clean:
	rm -f $(OBJ)/*.o $(OBJDIR_gnu64)/*.o $(OBJDIR_test)/*.o
mrproper: clean
	rm -f build/bin/duck-gen build/bin/duck-gen-test
.PHONY: gnu64 run-gnu64 install-gnu64 uninstall-gnu64 test run-test install-test uninstall-test check clean mrproper
