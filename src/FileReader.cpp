#include <fstream>
#include <FileReader.hpp>

FileReader::FileReader()
{
  
}

/*virtual*/ std::string FileReader::read(std::string const& filename)
{
  std::ifstream file(filename);

  if (!file)
    {
      throw std::runtime_error("Cannot open " + filename);
    }
  
  std::string line;
  std::string content;

  while ( std::getline(file, line) )
    {
      content += line;

      if ( !file.eof() )
	{
	  content += "\n";
	}
    }
  
  return content;
}

FileReader::~FileReader()
{
  
}
