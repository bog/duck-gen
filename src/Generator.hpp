#ifndef GENERATOR_HPP
#define GENERATOR_HPP
#include <vector>
#include <iostream>

class FileWriter;
class Template;

class Generator
{
public:
  explicit Generator(Template& temp, FileWriter& writer);
  void generate();
  std::vector<std::string> getVars();
  std::vector<std::pair<std::string, std::string>> getConds();
  virtual ~Generator();
  
protected:
  
private:
  FileWriter& m_writer;
  Template& m_temp;
  Generator( Generator const& generator ) = delete;
  Generator& operator=( Generator const& generator ) = delete;
};

#endif
