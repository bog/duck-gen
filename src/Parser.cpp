#include <regex>
#include <Parser.hpp>
#include "Template.hpp"
#include "FileReader.hpp"

Parser::Parser(FileReader& reader)
  : m_reader { reader }
{
  
}

Template Parser::parse(std::string const& filename)
{
  Template t;
  std::string file_content = m_reader.read(filename);
  m_file_content = file_content;
  /* Conds */
  {
    std::smatch match;
    std::string content = file_content;
    do
      {
	if (std::regex_search(content,
			      match,
			      std::regex("@([a-zA-Z0-9\\-_]+)\\(\\(([@~(){}a-zA-Z0-9\\-_\n\r\t ]*?)\\)\\)")))
	  {
	    t.addCond(match[1].str(), match[2].str());
	    content = match.suffix();
	  }
	else
	  {
	    content = "";
	  }
      }
    while ( !content.empty() );    
  }

  /* Vars */
  {
    std::smatch match;
    std::string content = file_content;
    do
      {
	if (std::regex_search(content,
			      match,
			      std::regex("@([a-zA-Z0-9\\-_]+)\\s+")))
	  {
	    t.addVar(match[1].str());
	    content = match.suffix();
	  }
	else
	  {
	    content = "";
	  }
      }
    while ( !content.empty() );    
  }

  return t;
}

std::string Parser::getFileContent() const
{
  return m_file_content;
}

Parser::~Parser()
{
  
}
