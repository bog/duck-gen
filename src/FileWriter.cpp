#include <filesystem>
#include <fstream>
#include <FileWriter.hpp>

FileWriter::FileWriter()
{
  
}

/*virtual*/ void FileWriter::write(std::string const& filename, std::string const& content)
{
  std::ofstream file { filename };

  if (!file)
    {
      throw std::runtime_error("Cannot find file " + filename);
    }

  file << content;
}

/*virtual*/ void FileWriter::createDir(std::string const& dirname)
{
  std::filesystem::create_directories(dirname);
}

FileWriter::~FileWriter()
{
  
}
