#ifndef FILEWRITER_HPP
#define FILEWRITER_HPP
#include <iostream>
#include <fstream>

class FileWriter
{
public:
  explicit FileWriter();
  virtual void write(std::string const& filename, std::string const& content);
  virtual void createDir(std::string const& dirname);
  virtual ~FileWriter();
  
protected:
  
private:
  FileWriter( FileWriter const& filewriter ) = delete;
  FileWriter& operator=( FileWriter const& filewriter ) = delete;
};

#endif
