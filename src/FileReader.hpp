#ifndef FILEREADER_HPP
#define FILEREADER_HPP
#include <iostream>
#include <fstream>

class FileReader
{
public:
  explicit FileReader();

  virtual std::string read(std::string const& filename);
  
  virtual ~FileReader();
  
protected:
  
private:
  FileReader( FileReader const& filereader ) = delete;
  FileReader& operator=( FileReader const& filereader ) = delete;
};

#endif
