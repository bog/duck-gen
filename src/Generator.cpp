#include <Generator.hpp>
#include "FileWriter.hpp"
#include "Template.hpp"

Generator::Generator(Template& temp, FileWriter& writer)
  : m_temp { temp }
  , m_writer { writer }
{
  
}

void Generator::generate()
{
  for ( std::string const& dir : m_temp.getDirs() )
    {
      m_writer.createDir(dir);
    }

  for ( auto const& file : m_temp.getFiles() )
    {
      m_writer.write(file.first, file.second);
    }
}

std::vector<std::string> Generator::getVars()
{
  std::vector<std::string> res;

  for (std::string const& var : m_temp.getVars())
    {
      res.push_back(var);
    }
  return res;
}

std::vector<std::pair<std::string, std::string>> Generator::getConds()
{
  std::vector<std::pair<std::string, std::string>> res;
  
  for (auto const& x : m_temp.getConds())
    {
      res.push_back(x);
    }
  
  return res;
}

Generator::~Generator()
{
  
}
