#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP
#include <iostream>
#include <vector>
#include <unordered_map>

class Controller
{
public:
  explicit Controller(std::vector<std::string> args);
  void exec();
  virtual ~Controller();
  
protected:
  
private:
  std::vector<std::string> m_args;
  std::string m_asset_dir;
  std::unordered_map<std::string, std::string> m_values;
  
  Controller( Controller const& controller ) = delete;
  Controller& operator=( Controller const& controller ) = delete;
};

#endif
