#include <regex>
#include <filesystem>
#include <Controller.hpp>
#include "Parser.hpp"
#include "Generator.hpp"
#include "Template.hpp"
#include "FileReader.hpp"
#include "FileWriter.hpp"

Controller::Controller(std::vector<std::string> args)
  : m_args { args }
{
  m_asset_dir = "/usr/local/share/duck-gen/templates/";
  
  if ( !std::filesystem::exists(m_asset_dir) )
    {
      m_asset_dir = "./";
    }  
}

void Controller::exec()
{
  if (m_args.size() != 2)
    {
      std::cerr<< "Usage : " <<std::endl;
      std::cerr<< "\t"<< "duck-gen <section> <template_name>" <<std::endl;
    }
  else
    {
      std::string section = m_args[0];
      std::string template_name = m_args[1];
      
      FileReader reader;      
      Parser parser(reader);
      std::string path = m_asset_dir + "/" + section + "/" + template_name + ".dgt";

      if ( !std::filesystem::exists(path) )
	{
	  throw std::runtime_error(path + " does not exists");
	}
      
      Template temp = parser.parse(path);
      
      /* vars */
      std::map<std::string, std::string> vars_values;
      std::vector<std::string> vars = temp.getVars();
      vars.erase(std::unique(vars.begin(),
			     vars.end()),
		 vars.end());
      for ( std::string const& var : vars)
      	{
      	  std::cout<< var << " ? ";
      	  std::string v;
      	  std::cin >> v;
	  vars_values[var] = v;
      	}
      
      /* Conds */
      
      std::map<std::string, std::string> conds = temp.getConds();
      std::map<std::string, std::string> conds_values;
      for (auto& p : conds)
	{
	  std::cout<< p.first << "? (Y/n) ";
	  std::string v = "n";
	  std::cin >> v;
	  std::cout << std::endl;

	  conds_values[p.first] = v;
	}

      std::string content = parser.getFileContent();

      do
	{
	  for (auto const& p : vars_values)
	    {
	      content = std::regex_replace(content, std::regex("@" + p.first), p.second);
	    }
	  for (auto const& p : conds_values)
	    {
	      std::regex reg {"@" + p.first + "\\(\\([~(){}a-zA-Z0-9\\-_\n\r\t ]*\\)\\)"};
	  
	      if (p.second == "n")
		{	 
		  content = std::regex_replace(content, reg, "");
		}
	      else
		{
		  content = std::regex_replace(content, reg, conds[p.first]);
		}
	    }
	}
      while (std::regex_search(content, std::regex("@")));

      // Dir
      {
	std::smatch match;
	std::string str = content;

	do
	  {
	    if (std::regex_search(str, match, std::regex("###\\s(.*)")))
	      {
		temp.addDir(match[1]);
		str = match.suffix();
	      }
	    else
	      {
		str = "";
	      }
	  }
	while( !str.empty() );

      }

      // File
      {
	std::smatch match;
	std::string str = content;

	do
	  {
	    if (std::regex_search(str,
				  match,
				  std::regex("%%%\\s(.*)([^%]*)")))
	      {		
		temp.addFile(match[1], match[2]);
		str = match.suffix();
	      }
	    else
	      {
		str = "";
	      }
	  }
	while( !str.empty() );
      }

      std::cout<< content <<std::endl;

      /* Generation */
      FileWriter writer;
      Generator generator(temp, writer);
      generator.generate();
    }
}

Controller::~Controller()
{
  
}
