#ifndef TEMPLATE_HPP
#define TEMPLATE_HPP
#include <iostream>
#include <vector>
#include <map>

class Template
{
public:
  explicit Template();
  virtual ~Template();

  std::vector<std::string> getDirs() const;
  std::map<std::string, std::string> getFiles() const;
  std::vector<std::string> getVars() const;
  std::map<std::string, std::string> getConds() const;
  
  void addDir(std::string name);
  void addFile(std::string name, std::string content);
  void addVar(std::string name);
  void addCond(std::string name, std::string value);
protected:
  
private:
  std::vector<std::string> m_dirs;
  std::map<std::string, std::string> m_files;
  std::vector<std::string> m_vars;  
  std::map<std::string, std::string> m_conds;
};

#endif
