#include <regex>
#include <Template.hpp>

Template::Template()
{
  
}


std::vector<std::string> Template::getDirs() const
{
  return m_dirs;
}

std::map<std::string, std::string> Template::getFiles() const
{
  return m_files;
}

std::vector<std::string> Template::getVars() const
{
  return m_vars;
}

std::map<std::string, std::string> Template::getConds() const
{
  return m_conds;
}
  
void Template::addDir(std::string name)
{
  m_dirs.push_back(name);
}

void Template::addFile(std::string name, std::string content)
{
  m_files[name] = content;
}

void Template::addVar(std::string name)
{
  auto itr = std::find(m_vars.begin(), m_vars.end(), name);

  if (itr == m_vars.end())
    {
      m_vars.push_back(name);
    }
}

void Template::addCond(std::string name, std::string value)
{
  m_conds[name] = value;
}

Template::~Template()
{
  
}
