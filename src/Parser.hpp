#ifndef PARSER_HPP
#define PARSER_HPP
#include <iostream>

class Template;
class FileReader;

class Parser
{
public:
  explicit Parser(FileReader& reader);
  virtual ~Parser();

  Template parse(std::string const& filename);
  std::string getFileContent() const;
protected:
  
private:
  FileReader& m_reader;
  std::string m_file_content;
  std::string m_file_name;
  
  Parser( Parser const& parser ) = delete;
  Parser& operator=( Parser const& parser ) = delete;
};

#endif
