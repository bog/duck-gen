#include <iostream>
#include "Controller.hpp"

int main(int argc, char** argv)
{
  std::vector<std::string> args;

  for (size_t i=1; i<argc; i++)
    {
      args.push_back(argv[i]);
    }
  
  Controller controller(args);
  controller.exec();
  
  return 0;
}
